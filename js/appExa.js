function buscarCoctel() {
    const nombreCoctel = document.getElementById('txtNombre').value;
  
    // Verificar si se ingresó un nombre
    if (nombreCoctel.trim() === '') {
      alert('Ingrese un nombre de coctel válido.');
      return;
    }
  
 
    axios.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${nombreCoctel}`)
      .then(response => {
        const coctel = response.data.drinks[0];
  
        if (coctel) {
          mostrarResultado(coctel);
        } else {
          alert('Coctel no encontrado.');
          limpiarCampos();
        }
      })
      .catch(error => {
        console.error('Error al consumir el API:', error);
      });
  }
  
  function mostrarResultado(coctel) {
    const resultadoDiv = document.getElementById('resultado');
    resultadoDiv.innerHTML = `
      <h2>${coctel.strDrink}</h2>
      <p>Categoría: ${coctel.strCategory}</p>
      <p>Instrucciones: ${coctel.strInstructions}</p>
      <img src="${coctel.strDrinkThumb}" alt="${coctel.strDrink}">
    `;
  }
  
  function limpiarCampos() {
    document.getElementById('txtNombre').value = '';
    document.getElementById('resultado').innerHTML = '';
  }
  